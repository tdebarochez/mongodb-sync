#!/bin/bash

# Docker auto populated vars
MONGODB_HOST=${MONGODB_RESTORE_PORT_27017_TCP_ADDR:-${MONGODB_RESTORE_HOST}}
MONGODB_HOST=${MONGODB_RESTORE_PORT_1_27017_TCP_ADDR:-${MONGODB_RESTORE_HOST}}
MONGODB_PORT=${MONGODB_RESTORE_PORT_27017_TCP_PORT:-${MONGODB_RESTORE_PORT}}
MONGODB_PORT=${MONGODB_RESTORE_PORT_1_27017_TCP_PORT:-${MONGODB_RESTORE_PORT}}

MONGODB_USER=${MONGODB_RESTORE_USER:-${MONGODB_RESTORE_ENV_MONGODB_USER}}
MONGODB_PASS=${MONGODB_RESTORE_PASS:-${MONGODB_RESTORE_ENV_MONGODB_PASS}}

MONGODB_URI=${MONGODB_URI}

RESTORE_CMD="mongorestore --drop"


# Building CMD
if [[ -n "${MONGODB_URI}" ]]; then
    # Since MongoDump 3.4.6.
    # https://docs.mongodb.com/manual/reference/program/mongodump/#cmdoption-mongodump-uri
    RESTORE_CMD="${RESTORE_CMD} --uri ${MONGODB_URI}"
else
    [[ ( -z "${MONGODB_RESTORE_USER}" ) && ( -n "${MONGODB_RESTORE_PASS}" ) ]] && MONGODB_RESTORE_USER='admin'

    [[ ( -n "${MONGODB_RESTORE_USER}" ) ]] && USER_RESTORE_STR=" --username ${MONGODB_RESTORE_USER}"
    [[ ( -n "${MONGODB_RESTORE_PASS}" ) ]] && PASS_RESTORE_STR=" --password ${MONGODB_RESTORE_PASS}"
    [[ ( -n "${MONGODB_RESTORE_DB}" ) ]] && DB_RESTORE_STR=" --db ${MONGODB_RESTORE_DB}"

    RESTORE_CMD="${RESTORE_CMD} --host ${MONGODB_RESTORE_HOST} --port ${MONGODB_RESTORE_PORT} ${USER_RESTORE_STR}${PASS_RESTORE_STR}${DB_RESTORE_STR}"
fi


FILE_TO_RESTORE=${1}
RESTORE_CMD="${RESTORE_CMD} /backup/$FILE_TO_RESTORE"


[[ -z "${1}" ]] && FILE_TO_RESTORE=$(ls /backup -N1 | grep -iv ".tgz" | sort -r | head -n 1)

echo "=> Restore database from ${FILE_TO_RESTORE}"
if ${RESTORE_CMD}; then
    echo "   Restore succeeded"
else
    echo "   Restore failed"
fi
echo "=> Done"
